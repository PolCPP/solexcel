#SolExcel

SolExcel is a compact accounting excel importer for ContaSol. It uses a simple to fill format to create accounting movements easily that can be turned into ContaSol format.

##Status

Functional except taxes

##Requirements

* Php 5.3+ server

##Installation

* Upload it to a php server
* Install composer requirements
* Make sure you add cron job to clean the xls files at the files directory.

