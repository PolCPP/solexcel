#SolExcel

SolExcel es un sistema compacto para generar importaciones para ContaSol. Utiliza un formato en excel sencillo de rellenar a mano para luego transformarlo a un formato que entiende Contasol.

##Estado

Funcional excepto impuestos (iva/igic) que funcionan parcialmente (no se rellenan los libros de iva, pero se genera el apunte en contabilidad).

##Uso

* Descargar la plantilla. Esta tiene los siguientes ejemplos
	* Asiento doble
	* Asiento de compra con impuesto
	* Asiento de devolución de compra
	* Asiento de venta
	* Devolución venta
		
	** Importante ** La columna **O** nunca debe ser rellenada sino generada con la formula de los ejemplos

	En la segunda página de la plantilla hay diferentes variables de configuración, como son la cuentas de iva/igic y el texto para los movimientos de pago.
	
* Una vez la plantilla esta preparada, visitamos la web de SolExcel y hacemos click en *genera la importación*, hacemos click en *selecciona el fichero* buscamos el fichero de excel necesario y hacemos click en *Generar la Plantilla*

Una vez generado nos dará la opción de descargar el fichero de importación en contasol.

* Para importar en contasol.
	* Renombramos el fichero que hemos descargado a **APU.xls** y lo copiamos a un directorio en vacio
	* Abrimos la empresa y ejercicio correspondiente donde queremos importar 	
	* Vamos a la pestaña **Utilidades** y hacemos click en el botón **Archivos > XLS** de la cinta.
	* En la pantalla que nos aparece seleccionamos el directorio que hemos creado que contiene el fichero APU.xls y solo marcamos la opción Diario de movimientos.
	* Hacemos clic en aceptar y generará la importación.
	* Una vez hecha la importación Regeneramos saldos *(Pestaña **Diario/IVA**, el botón de la esquina superior derecha)* y revisamos los movimientos, a partir del asiento 15000.
	* Si se van a hacer posteriores importaciones hacer una renumeración de asientos antes de importar.
	
Los movimientos se importan en el asiento 15.000 en adelante, para poder identificarlos y eliminarlos de forma sencilla si ha habido algun error con la importación o se quiere eliminar esa importación.

**Si hubiera algun error el programa nos abrirá una carpeta con un fichero especificando los errores.**
	
** Importante: es necesario disponer de Microsoft Excel o Openoffice/LibreOffice instalados para realizar la importación**