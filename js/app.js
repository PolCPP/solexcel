(function ($) {

	var bar = $('.bar');
	var percent = $('.percent');
	var status = $('#status');

	function loadImport() {
	$(".intro").slideUp(
		function() {
			$(".import").slideDown();
		});
	}

	function init() {
		$(".generate").click(loadImport);
		$('input[type=file]').bootstrapFileInput();

		$('form').ajaxForm({
			beforeSend: function() {
				status.empty();
				var percentVal = '0%';
				bar.width(percentVal);
				percent.html(percentVal);
			},
			uploadProgress: function(event, position, total, percentComplete) {
				var percentVal = percentComplete + '%';
				bar.width(percentVal);
				percent.html(percentVal);
			},
			success: function() {
				var percentVal = '100%';
				bar.width(percentVal);
				percent.html(percentVal);
			},
			complete: function(xhr) {
				$(".import .btn").fadeOut(); $("form").fadeOut();
				status.html(xhr.responseText);
			}
		});
	}

	$(document).ready(function() {
		init();
	});
})(jQuery);