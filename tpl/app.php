
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Pol Cámara | Soft10">
    <link rel="icon" href="../favicon.ico">

    <title>SolExcel | La contabilidad eficiente</title>

    <!-- Bootstrap core CSS -->
    <link href="../dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/cover.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../assets/js/ie-emulation-modes-warning.js"></script>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../assets/js/ie10-viewport-bug-workaround.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="site-wrapper">

      <div class="site-wrapper-inner">

        <div class="cover-container">

          <div class="masthead clearfix">
            <div class="inner">
              <h3 class="masthead-brand">
                <span>Sol</span>Excel</h3>
            </div>
          </div>

          <div class="inner cover intro">
            <h1 class="cover-heading">La contabilidad eficiente</h1>
            <p class="lead">SolExcel es un sistema para facilitar la creación de movimentos contables utilizando hojas de cálculo</p>
            <p class="lead">
              <a href="/docs/manual.pdf" class="btn btn-lg btn-info">Descarga el manual</a>
              <a href="/docs/template.xls" class="btn btn-lg btn-info">Descarga la plantilla</a>    
            </p>
            <p class="lead">
              <a href="javascript:void(0)" class="generate btn btn-lg btn-success">Genera la importación</a>
            </p>
          </div>

          <div class="inner cover import">
            <h1 class="cover-heading">Genera una importación.</h1>
            <p class="lead">Carga la plantilla para generar la importación de Contasol.</p><p>No te preocupes, no almacenamos ni analizamos ningún dato. <br>Y si no estás seguro <a href="https://bitbucket.org/PolCPP/solexcel">siempre puedes instalarte la aplicación solamente para ti</a></p>
            <form id="file-upload" action="/" method="POST" enctype="multipart/form-data"> 
              <p class="lead">
                <input type="file" name="data" title="Selecciona el fichero">
              </p>
              <p class="lead do-generate">
                <input type="submit" class="btn btn-lg btn-success" value="Generar la plantilla">
              </p>
              <div class="progress">
                  <div class="bar"></div >
                  <div class="percent">0%</div >
              </div>              
            </form>
            <p id="status"></p>

          </div>



          <div class="mastfoot">
            <div class="inner">
              <p>Sol<strong>Excel</strong> es un producto <a target="_blank" href="https://bitbucket.org/PolCPP/solexcel">libre</a> de <a href="#">Soft10</a>.</p>
            </div>
          </div>

        </div>

      </div>

    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="../dist/js/bootstrap.min.js"></script>
    <script src="../dist/js/bootstrap.file-input.js"></script>    
    <script src="http://malsup.github.com/jquery.form.js"></script>     
    <script src="../js/app.js"></script>
  </body>
</html>

