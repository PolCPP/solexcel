<?php

Class Core {
	private $app;
	private $outputFile;
	private $outputHandle;
	private $currentRow;
	private $currentEntry;
	private $taxAccountS;
	private $taxAccountR;
	private $payText;
	private $filePath;
	private $settings;
	private $bookId;

	public function __construct($app, $settings) {
		$this->app = $app;
		$this->settings = $settings;
		$this->filePath = $settings["filePath"];
		$this->cleanFiles();
	}

	function showHome() {
		$this->app->render("app.php");  
	}

	function genTemplate() {
		$this->outputFile = new PHPExcel();
		$this->outputHandle = $this->outputFile->setActiveSheetIndex(0);
		$this->currentEntry = 15000;
		$this->currentRow = 1;

		// Some basic validation.
		if (!isset($_FILES["data"]) && sizeof($_FILES["data"]) == 0) {
			$this->quitError("not_found");
		}

		try {
			$inputFile = PHPExcel_IOFactory::load($_FILES["data"]["tmp_name"]);             
		} catch (Exception $e) {
			$this->quitError("error_file");
		}

		$this->loadSettings($inputFile);

		$sheet = $inputFile->getSheet(0); 
		$highestRow = $sheet->getHighestRow(); 
		$highestColumn = $sheet->getHighestColumn();
			
		$this->writeTitles();

		// We start from row 3. Row <=2 Will always titles.
		for ($row = 3; $row <= $highestRow; $row++) { 
			$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
												NULL,
												TRUE,
												FALSE);
			$this->parseRow($rowData[0]);
		}

		// Write the temp file
		$filename = $this->randomString();
		$objWriter = PHPExcel_IOFactory::createWriter($this->outputFile, 'Excel5');
		$objWriter->save($this->filePath.$filename.".xls");
					
		// Offer it to download
		echo sprintf($this->settings['messages']['success'], $filename); 
	}

	function download($id) {
		$f = $this->filePath . $id . '.xls';
		if (file_exists($f)) {
			header('Content-type:  application/octet-stream');
			header('Content-Length: ' . filesize($f));
			header("Content-Disposition: attachment; filename='apu.xls'");
			header('Expires: 0');
			header('Cache-Control: must-revalidate');
			header('Pragma: public');		
			readfile($f);
			ignore_user_abort(true);
			unlink($f);			
		} else {
		    $this->app->pass();
		}
	}	

	function writeTitles() {
		$this->outputHandle->setCellValue('A'.$this->currentRow, 'Diario')
			->setCellValue('B'.$this->currentRow, 'Fecha  ')
			->setCellValue('C'.$this->currentRow, 'Asiento')
			->setCellValue('D'.$this->currentRow, 'Orden  ')
			->setCellValue('E'.$this->currentRow, 'Cuenta')
			->setCellValue('F'.$this->currentRow, 'Importe')
			->setCellValue('G'.$this->currentRow, 'Concepto')
			->setCellValue('H'.$this->currentRow, 'Documento')
			->setCellValue('I'.$this->currentRow, 'Importe deb')
			->setCellValue('J'.$this->currentRow, 'Importe haber')
			->setCellValue('K'.$this->currentRow, 'Moneda')
			->setCellValue('L'.$this->currentRow, 'Punteo')
			->setCellValue('M'.$this->currentRow, 'Tipo de I.V.A.')
			->setCellValue('N'.$this->currentRow, 'Código de I.V.A.')
			->setCellValue('O'.$this->currentRow, 'Departamento')
			->setCellValue('P'.$this->currentRow, 'Subdepartamento')
			->setCellValue('Q'.$this->currentRow, 'Archivo de imagen');
		$this->currentRow++;
	}

	function parseRow($row) {
		$mode = $row[0];
		if ($mode) {
			if ($mode == "AD") {
				$this->doDoubleMovement($row[1], $row[2], $row[5], $row[8], $row[15]);
			} else {
				$tax = array();
				$totalTax = 0;
				// First we check out if we have taxes
				for ($i = 9; $i < 14; $i++) {
					if ($row[$i] !== "" && $row[$i] > 0) {
						$tax[] = $row[$i];
						$totalTax += $row[$i];
					}				
				}
				$this->doPurchaseMovement($mode, $row[1], $row[2], $row[4], $row[5], $row[6], $row[7], $row[8], $row[15], $totalTax, $row[3]);
			}			
		}
	}

	function doDoubleMovement($debitAct,$creditAct,$date,$comment,$value) {
		$order = 0;
		$this->outputHandle->setCellValue('A'.$this->currentRow, $this->bookId)
			->setCellValue('B'.$this->currentRow, $date)
			->setCellValue('C'.$this->currentRow, $this->currentEntry)
			->setCellValue('D'.$this->currentRow, $order)
			->setCellValue('E'.$this->currentRow, $debitAct)
			->setCellValue('F'.$this->currentRow, 0)
			->setCellValue('G'.$this->currentRow, $comment)
			->setCellValue('H'.$this->currentRow, '')
			->setCellValue('I'.$this->currentRow, $value)
			->setCellValue('J'.$this->currentRow, 0)
			->setCellValue('K'.$this->currentRow, 'E')
			->setCellValue('L'.$this->currentRow, '0')
			->setCellValue('M'.$this->currentRow, '')
			->setCellValue('N'.$this->currentRow, '')
			->setCellValue('O'.$this->currentRow, '')
			->setCellValue('P'.$this->currentRow, '')
			->setCellValue('Q'.$this->currentRow, '')
			->getStyleByColumnAndRow(1, $this->currentRow)->getNumberFormat()->setFormatCode('dd/mm/yyyy');

		$this->currentRow++;
		$order++;

		$this->outputHandle->setCellValue('A'.$this->currentRow, $this->bookId)
			->setCellValue('B'.$this->currentRow, $date)
			->setCellValue('C'.$this->currentRow, $this->currentEntry)
			->setCellValue('D'.$this->currentRow, $order)
			->setCellValue('E'.$this->currentRow, $creditAct)
			->setCellValue('F'.$this->currentRow, 0)
			->setCellValue('G'.$this->currentRow, $comment)
			->setCellValue('H'.$this->currentRow, '')
			->setCellValue('I'.$this->currentRow, 0)
			->setCellValue('J'.$this->currentRow, $value)
			->setCellValue('K'.$this->currentRow, 'E')
			->setCellValue('L'.$this->currentRow, '0')
			->setCellValue('M'.$this->currentRow, '')
			->setCellValue('N'.$this->currentRow, '')
			->setCellValue('O'.$this->currentRow, '')
			->setCellValue('P'.$this->currentRow, '')
			->setCellValue('Q'.$this->currentRow, '')
			->getStyleByColumnAndRow(1, $this->currentRow)->getNumberFormat()->setFormatCode('dd/mm/yyyy');
		$this->currentRow++;
		$this->currentEntry++;
	}

	function doPurchaseMovement($mode, $debitAct, $creditAct, $payAct, $date, $payDate, $document, $comment, $value, $tax, $taxType) {
		$order = 0;
		$totalDebit = $value;		
		$totalCredit = $value;
		$taxAccount = "";

		if ($tax != 0) {
			if ($taxType == "R") {
				$taxAccount = $this->taxAccountR;
			} else {
				$taxAccount = $this->taxAccountS;				
			}

			if ($mode == "C" || $mode == "DV") {
				$totalDebit -= $tax;
			} else {
				$totalCredit -= $tax;
			}	
		}

		$this->outputHandle->setCellValue('A'.$this->currentRow, $this->bookId)
			->setCellValue('B'.$this->currentRow, $date)
			->setCellValue('C'.$this->currentRow, $this->currentEntry)
			->setCellValue('D'.$this->currentRow, $order)
			->setCellValue('E'.$this->currentRow, $debitAct)
			->setCellValue('F'.$this->currentRow, 0)
			->setCellValue('G'.$this->currentRow, $comment)
			->setCellValue('H'.$this->currentRow, $document)
			->setCellValue('I'.$this->currentRow, $totalDebit)
			->setCellValue('J'.$this->currentRow, 0)
			->setCellValue('K'.$this->currentRow, 'E')
			->setCellValue('L'.$this->currentRow, '0')
			->setCellValue('M'.$this->currentRow, '')
			->setCellValue('N'.$this->currentRow, '')
			->setCellValue('O'.$this->currentRow, '')
			->setCellValue('P'.$this->currentRow, '')
			->setCellValue('Q'.$this->currentRow, '')
			->getStyleByColumnAndRow(1, $this->currentRow)->getNumberFormat()->setFormatCode('dd/mm/yyyy');

		$this->currentRow++;
		$order++;


		if ($tax != 0) {
			if ($mode == "C" || $mode == "DV") {
				$this->outputHandle->setCellValue('A'.$this->currentRow, $this->bookId)
					->setCellValue('B'.$this->currentRow, $date)
					->setCellValue('C'.$this->currentRow, $this->currentEntry)
					->setCellValue('D'.$this->currentRow, $order)
					->setCellValue('E'.$this->currentRow, $taxAccount)
					->setCellValue('F'.$this->currentRow, 0)
					->setCellValue('G'.$this->currentRow, $comment)
					->setCellValue('H'.$this->currentRow, $document)
					->setCellValue('I'.$this->currentRow, $tax)
					->setCellValue('J'.$this->currentRow, 0)
					->setCellValue('K'.$this->currentRow, 'E')
					->setCellValue('L'.$this->currentRow, '0')
					->setCellValue('M'.$this->currentRow, '')
					->setCellValue('N'.$this->currentRow, '')
					->setCellValue('O'.$this->currentRow, '')
					->setCellValue('P'.$this->currentRow, '')
					->setCellValue('Q'.$this->currentRow, '')
					->getStyleByColumnAndRow(1, $this->currentRow)->getNumberFormat()->setFormatCode('dd/mm/yyyy');				
			} else {
				$this->outputHandle->setCellValue('A'.$this->currentRow, $this->bookId)
					->setCellValue('B'.$this->currentRow, $date)
					->setCellValue('C'.$this->currentRow, $this->currentEntry)
					->setCellValue('D'.$this->currentRow, $order)
					->setCellValue('E'.$this->currentRow, $taxAccount)
					->setCellValue('F'.$this->currentRow, 0)
					->setCellValue('G'.$this->currentRow, $comment)
					->setCellValue('H'.$this->currentRow, $document)
					->setCellValue('I'.$this->currentRow, 0)
					->setCellValue('J'.$this->currentRow, $tax)
					->setCellValue('K'.$this->currentRow, 'E')
					->setCellValue('L'.$this->currentRow, '0')
					->setCellValue('M'.$this->currentRow, '')
					->setCellValue('N'.$this->currentRow, '')
					->setCellValue('O'.$this->currentRow, '')
					->setCellValue('P'.$this->currentRow, '')
					->setCellValue('Q'.$this->currentRow, '')
					->getStyleByColumnAndRow(1, $this->currentRow)->getNumberFormat()->setFormatCode('dd/mm/yyyy');
			}	
			$this->currentRow++;
			$order++;			
		}

		$this->outputHandle->setCellValue('A'.$this->currentRow, $this->bookId)
			->setCellValue('B'.$this->currentRow, $date)
			->setCellValue('C'.$this->currentRow, $this->currentEntry)
			->setCellValue('D'.$this->currentRow, $order)
			->setCellValue('E'.$this->currentRow, $creditAct)
			->setCellValue('F'.$this->currentRow, 0)
			->setCellValue('G'.$this->currentRow, $comment)
			->setCellValue('H'.$this->currentRow, $document)
			->setCellValue('I'.$this->currentRow, 0)
			->setCellValue('J'.$this->currentRow, $value)
			->setCellValue('K'.$this->currentRow, 'E')
			->setCellValue('L'.$this->currentRow, '0')
			->setCellValue('M'.$this->currentRow, '')
			->setCellValue('N'.$this->currentRow, '')
			->setCellValue('O'.$this->currentRow, '')
			->setCellValue('P'.$this->currentRow, '')
			->setCellValue('Q'.$this->currentRow, '')
			->getStyleByColumnAndRow(1, $this->currentRow)->getNumberFormat()->setFormatCode('dd/mm/yyyy');
		$this->currentRow++;
		$this->currentEntry++;

		// Do payment as double movement.
		if ($payDate) {
			if ($mode == "C" || $mode == "DV") {
				$this->doDoubleMovement($creditAct,$payAct,$payDate,$this->payText.$comment,$value);
			} else {
				$this->doDoubleMovement($payAct,$debitAct,$payDate,$this->payText.$comment,$value);
			}				
		}
	}

	function quitError($errorCode) {
		echo $this->settings['messages'][$errorCode];
		exit();
	}

	function randomString($length = 10) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, strlen($characters) - 1)];
		}
		return $randomString;
	}

	function loadSettings($inputFile) {
		$sheet = $inputFile->getSheet(1);
		$settings = $sheet->rangeToArray('B1:B4');
		$this->taxAccountS = $settings[0][0]; 
		$this->taxAccountR = $settings[1][0]; 
		$this->payText = $settings[2][0];
		$this->bookId = $settings[3][0];
	}

	function cleanFiles() {
		foreach (glob($this->filePath."*") as $file) {
			if (filemtime($file) < time() - 300) {
				unlink($file);
			}
		}
	}
	
}
