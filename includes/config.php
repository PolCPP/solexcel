<?php
	$settings = array( 
		'messages' => array( 
			'not_found' => "No se ha encontrado fichero para transformar",
			'error_file' => "El fichero no se puede abrir",
			'success' => "Fichero generado, puede descargarlo haciendo click en el siguiente enlace <br> <a href='/download/%s' class='btn btn-success'>Descargar</a>"
		),
		'filePath' => "files/",
	); 