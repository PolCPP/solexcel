<?php
session_cache_limiter(false);

require_once 'includes/config.php';
require 'vendor/autoload.php';
require 'includes/core.php';


$app = new \Slim\Slim(
	array(
		'debug' => true ,
		'templates.path' => 'tpl'
	)
);
$core = new Core($app, $settings);

$app->get('/', function () use ($core) {
	$core->showHome();	
});

$app->post('/', function () use ($core) {
	$core->genTemplate();
});

$app->get('/download/:id', function ($id) use ($core) {
	$core->download($id);
});

$app->get('/test', function () use ($core) {
	$_FILES = array("data" => array("tmp_name" => "test.xlsx")); 
	$core->genTemplate();
});


$app->run();
